import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview/constants/ui_constants.dart';

class ClosingButton extends StatelessWidget {
  final Color iconColor;
  final Color backgroundColor;
  final Function? call;

  const ClosingButton({
    this.iconColor = UIConstants.accentColor,
    this.backgroundColor = UIConstants.primaryColor,
    this.call,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () => call ?? Navigator.pop(context),
        child: Container(
          width: width * 0.095,
          height: width * 0.095,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: backgroundColor,
            border: Border.all(color: UIConstants.paleLilac),
          ),
          child: Icon(
            Icons.close,
            color: iconColor,
            size: width * 0.05,
          ),
        ),
      ),
    );
  }
}
