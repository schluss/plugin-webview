import 'package:flutter/material.dart';
import 'package:webview/constants/ui_constants.dart';

class MainButton extends StatelessWidget {
  final String buttonText;
  final Function call;
  final Color? backgroundColor;
  final Color? textColor;
  final double widthFactor;
  final bool isTextBold;
  final Color? borderColor;

  const MainButton(
    this.buttonText,
    this.call, {
    this.backgroundColor,
    this.borderColor,
    this.textColor,
    this.widthFactor = 0.15,
    this.isTextBold = false,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width * widthFactor,
      child: Container(
        decoration: (backgroundColor == null)
            ? BoxDecoration(
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    spreadRadius: -9,
                    color: UIConstants.watermelon,
                    blurRadius: 18,
                    offset: Offset(0, 8),
                  ),
                ],
              )
            : null,
        child: RaisedButton(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(
              color: (borderColor == null) ? getBackgroundColor(context) : borderColor!,
            ),
          ),
          onPressed: call as void Function()?,
          color: getBackgroundColor(context),
          textColor: (textColor == null) ? Colors.white : textColor,
          splashColor: Colors.transparent,
          child: buildText(context),
        ),
      ),
    );
  }

  Color getBackgroundColor(BuildContext context) {
    return (backgroundColor == null) ? Theme.of(context).accentColor : backgroundColor!;
  }

  Text buildText(BuildContext context) {
    return Text(
      buttonText,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: MediaQuery.of(context).size.width * 0.045,
        fontWeight: (isTextBold) ? FontWeight.w600 : null,
      ),
    );
  }
}
