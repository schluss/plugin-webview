import 'package:flutter/material.dart';
import 'ui_constants.dart';

final appTheme = ThemeData(
  accentColor: UIConstants.accentColor,
  primaryColor: UIConstants.primaryColor,
  fontFamily: UIConstants.defaultFontFamily,
  textTheme: TextTheme(
    headline5: TextStyle(
      fontFamily: UIConstants.defaultFontFamily,
      fontWeight: FontWeight.w700,
      fontSize: 24,
      color: Colors.black,
    ),
  ),
);
