import 'package:flutter/material.dart';

import '../constants/ui_constants.dart';
import 'loading_indicator.dart';
import 'mini_header_template.dart';

Locale? _locale;
BuildContext? _context;

class WaitOverlay {
  static void hide() {
    try {
      if (Navigator.of(_context!, rootNavigator: true).canPop()) {
        Navigator.of(_context!, rootNavigator: true).pop();
      } else {
        print('Unable to close webview.waiter. Please Review the Navigator Flow.');
      }
    } catch (e) {
      print(e);
    }
  }

  static void show(BuildContext context, Locale locale, {required String text}) {
    _context = context;
    var width = MediaQuery.of(_context!).size.width;
    var height = MediaQuery.of(_context!).size.height;

    text = '';

    Dialog alert;
    alert = Dialog(
        insetPadding: EdgeInsets.zero,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + height * 0.1,
                ),
                height: height,
                width: width,
                color: UIConstants.primaryColor,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: height * 0.25,
                  ),
                  child: LoadingIndicator(text: text),
                ),
              ),
              Stack(
                children: <Widget>[
                  Container(
                    color: UIConstants.paleLilac,
                    child: MiniHeaderTemplate(locale: locale),
                  ),
                ],
              ),
            ],
          ),
        ));

    showDialog(
      barrierDismissible: false,
      context: _context!,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
