abstract class CommonPluginDAO {
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'UWV',
  });

  Future<Map<String, dynamic>> getExtractedData(String filePath);
}
