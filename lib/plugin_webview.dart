library webview;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview/ui/browser.dart';

class PluginWebview {
  final StreamController<int> _streamController = StreamController.broadcast();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static Function? getEnvVar;
  static Function? storeInSecureStore;
  static Function? getFromSecureStore;

  static Locale? locale;

  // @override
  void runPlugin(context, Function callBack, String siteUrl, String trigger, {String pluginName = 'Webview'}) {
    locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Browser(callBack, siteUrl, trigger, pluginName: pluginName),
      ),
    );
  }

  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(100);
    return Future.value(jsonDecode(jsonData));
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
