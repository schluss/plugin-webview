import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:telephony/telephony.dart';
import 'package:webview/constants/ui_constants.dart';
import 'package:webview/plugin_webview.dart';
import 'package:webview/reused_widgets/mini_header_template.dart';
import 'package:webview/reused_widgets/wait_overlay.dart';

class Browser extends StatefulWidget {
  final Function callBack; // function to callback main application
  final String siteUrl;
  final String trigger;
  final String? pluginName;
  final Function(String userName, String password)? storeCredentialCallback;
  final String? userName;
  final String? password;

  Browser(
    this.callBack,
    this.siteUrl,
    this.trigger, {
    this.pluginName,
    this.storeCredentialCallback,
    this.userName,
    this.password,
  });

  @override
  _Browser createState() => _Browser();
}

class _Browser extends State<Browser> {
  final telephony = Telephony.instance;
  bool _hasSMSPermission = false;
  var _url;
  String? pluginScriptBody;

  /// This is used to show or hide the processing screen.
  bool _isShown = false;

  _Browser() {}

  /// Shows the processing screen.
  void showWaiter() async {
    if (!_isShown) {
      WaitOverlay.show(context, PluginWebview.locale as Locale, text: 'laden...');
      _isShown = true;
    }
  }

  /// Hides the processing screen.
  void hideWaiter() {
    if (_isShown) {
      WaitOverlay.hide();
      _isShown = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.siteUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var loadingText = 'Laden...';
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: InAppWebView(
                initialUrlRequest: URLRequest(url: Uri.parse(widget.siteUrl)),
                initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                        cacheEnabled: false, // Changes to true or false to enable or disable cache.
                        clearCache: true, // Clears all the webview's cache.
                        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36')),
                onWebViewCreated: (InAppWebViewController controller) async {
                  showWaiter();

                  /// (down)load the pluginScriptBody
                  var injectScriptUrl = 'https://services.schluss.app/belastingdienst/test/plugin.js'; //Todo: this could come from: pluginsettings.trigger (for now i cannot change this as it would change the behaviour of the current plugins)
                  injectScriptUrl += '?' + DateTime.now().millisecondsSinceEpoch.toString();
                  print('Getting plugin.js at: ' + injectScriptUrl);
                  var response = await Dio().get(injectScriptUrl); // added milliseconds to really make sure it is not cached
                  pluginScriptBody = response.data.toString();

                  /// register the javascript event handlers
                  controller.addJavaScriptHandler(
                      handlerName: 'requestSMSpermission',
                      callback: (args) async {
                        print('requestSMSpermission');
                        // request sms permissions here
                        _hasSMSPermission = await telephony.requestPhoneAndSmsPermissions as bool;

                        // return result back to js
                        return _hasSMSPermission;
                      });

                  controller.addJavaScriptHandler(
                      handlerName: 'listenForIncomingSMS',
                      callback: (args) async {
                        print('listenForIncomingSMS');

                        final completer = Completer<Map>();

                        /// do we have permission to listen?
                        if (!_hasSMSPermission) {
                          return false;
                        }

                        telephony.listenIncomingSms(
                            onNewMessage: (SmsMessage message) {
                              // Send back the result by completing the Completer
                              completer.complete({
                                'message': message.body,
                                'sender': message.address,
                              });

                              /*
                          return {
                            'message': message.body,
                            'sender': message.address,
                          };
                          */
                            },
                            listenInBackground: false);

                        return completer.future;
                      });

                  //todo: get stored credentials (in a generic way)
                  controller.addJavaScriptHandler(
                      handlerName: 'requestEnvVar',
                      callback: (args) async {
                        print('requestEnvVar');

                        // return result back to js
                        return PluginWebview.getEnvVar!(args[0]);
                      });

                  controller.addJavaScriptHandler(
                      handlerName: 'requestFromSecureStore',
                      callback: (args) async {
                        print('requestFromSecureStore');

                        // return result back to js
                        return PluginWebview.getFromSecureStore!(args[0]);
                      });

                  controller.addJavaScriptHandler(
                      handlerName: 'storeInSecureStore',
                      callback: (args) async {
                        print('storeInSecureStore');

                        // return result back to js
                        return PluginWebview.storeInSecureStore!(args[0], args[1]);
                      });

                  controller.addJavaScriptHandler(
                      handlerName: 'showWaiter',
                      callback: (args) {
                        showWaiter();
                        print('showWaiter');
                        return true;
                      });

                  controller.addJavaScriptHandler(
                      handlerName: 'hideWaiter',
                      callback: (args) {
                        hideWaiter();
                        print('hideWaiter');
                        return true;
                      });
                },
                onLoadStart: (controller, url) async {},
                onLoadStop: (InAppWebViewController controller, url) async {
                  print('loaded url: ' + url.toString());

                  var jsResult = await (controller.callAsyncJavaScript(functionBody: pluginScriptBody!, arguments: {'url': url.toString()}));

                  /* 
                  jsResult should have the following structure:
                  {
                    status : ok/error/done,
                    message : 'when there are errors to display',
                    attributes : {att1 :val1, att2 : val2}  // when status = done 
                  }
                  */

                  //Todo: handle errors etc.

                  if (jsResult!.error != null) {
                    throw (jsResult.error.toString());
                  }

                  print(jsResult!);

                  // when the status is done, the attributes are returned, so we send them back to the app
                  if (jsResult.value['status'] == 'done') {
                    widget.callBack(jsonEncode(jsResult.value['attributes']), context);
                  }
                },
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  title: _url,
                  locale: PluginWebview.locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
