import 'package:flutter/material.dart';
import 'package:webview/constants/ui_constants.dart';
import 'package:webview/reused_widgets/backward_button.dart';
import 'package:webview/reused_widgets/closing_button.dart';

class MiniHeaderTemplate extends StatelessWidget {
  final String? iconPath;
  final String title;
  final Color titleColor;
  final Locale? locale;
  final bool isDividerVisible;
  final Function? backBtnCall;
  final Function? closeBtnCall;

  MiniHeaderTemplate({
    this.iconPath,
    this.title = '',
    this.titleColor = UIConstants.accentColor,
    this.locale,
    this.isDividerVisible = true,
    this.backBtnCall,
    this.closeBtnCall,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      decoration: BoxDecoration(
        color: UIConstants.primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(20),
          topRight: const Radius.circular(20),
        ),
      ),
      height: height * 0.08,
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          /// Title.
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(left: height * 0.01),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: width * 0.615,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    (iconPath != null)
                        ? Image.asset(
                            iconPath!,
                            package: 'uwv',
                            height: width * 0.045,
                          )
                        : Container(),
                    Flexible(
                      child: Text(
                        title,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: titleColor,
                          fontSize: width * 0.04,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          /// Back button.
          Padding(
            padding: EdgeInsets.only(left: height * 0.01),
            child: BackwardButton(
              locale: locale!,
              call: backBtnCall,
            ),
          ),

          /// Close button.
          Padding(
            padding: EdgeInsets.only(right: height * 0.02),
            child: ClosingButton(
              call: closeBtnCall,
            ),
          ),

          /// Divider.
          isDividerVisible
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Divider(height: 1),
                )
              : Container(),
        ],
      ),
    );
  }
}
